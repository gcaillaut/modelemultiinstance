(TeX-add-style-hook
 "bib"
 (lambda ()
   (LaTeX-add-bibitems
    "belmandt2011basics"
    "largeron2002pretopological"
    "cleuziou2015qassit"
    "dietterich1997solving"
    "cleuziou2015structuration"))
 :bibtex)

