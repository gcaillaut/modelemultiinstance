(TeX-add-style-hook
 "modele"
 (lambda ()
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (LaTeX-add-labels
    "fig:relationsvoisinages"
    "fig:structureobjectif"
    "fig:pretopoobjectif"
    "fig:tabapprentissage1"
    "fig:memestructuration"
    "fig:tableauapprentissageadh1"
    "fig:tableauapprentissageadh"))
 :latex)

