\section{Définition du modèle}

Un espace prétopologique peut être induis à partir d'un ensemble $E$
et d'un ensemble de relations de voisinages $\cal N$. Notre objectif
est d'apprendre, de façon supervisée, une fonction d'adhérence
induisant un espace prétopologique dont la structuration s'approche
d'une connaissance partielle $S$, fournie en entrée de l'algorithme d'apprentissage.

Prenons pour exemple l'ensemble des relations de voisinages ${\cal N} = \{N_1,
N_2, N_3\}$ illustrées par la figure \ref{fig:relationsvoisinages},
ainsi que la structure objectif de la figure
\ref{fig:structureobjectif}. La figure \ref{fig:pretopoobjectif}
montre une représentation de l'espace prétopologique de la structure
objectif : l'élément $a$ subsume les éléments $b$, $c$ et $d$, tandis
que $c$ ne subsume que $d$.

\begin{figure}
  \centering

  \begin{minipage}{0.3\linewidth}
    \includegraphics[width=\textwidth]{N1.png}
    \caption*{${\cal N}_1$}
  \end{minipage}
  %
  \begin{minipage}{0.3\linewidth}
    \includegraphics[width=\textwidth]{N2.png}
    \caption*{${\cal N}_2$}
  \end{minipage}
  %
  \begin{minipage}{0.3\linewidth}
    \includegraphics[width=\textwidth]{N3.png}
    \caption*{${\cal N}_3$}
  \end{minipage}

  \caption{Trois relations de voisinage}
  \label{fig:relationsvoisinages}
\end{figure}


\begin{figure}
  \centering

  \begin{minipage}{0.4\linewidth}
    \includegraphics[width=0.9\linewidth]{objectif.png}
    \caption{Structure objectif}
    \label{fig:structureobjectif}
  \end{minipage}
  %
  \begin{minipage}{0.4\linewidth}
    \includegraphics[width=0.9\linewidth]{pretopoObjectif.png}
    \caption{Espace prétopologique objectif}
    \label{fig:pretopoobjectif}
  \end{minipage}
\end{figure}

Aucunes de ces relations ne permet, à elle seule, de déterminer la
structure finale. Cependant, chacune d'entre elles dispose
d'informations permettant d'induire une partie de la structure
objectif.
Une combinaison des ces relations de voisinage pourrait alors
permettre de retrouver la structure objectif.

La fonction d'adhérence peut être définie à partir de règles logiques,
c'est pourquoi nous cherchons à combiner les relations de voisinages
sous la forme d'une DNF \footnote{\emph{Disjunctive Normal Form}}, à
la manière de \citeauthor{cleuziou2015structuration}.
La fonction d'adhérence $a(.)$ s'écrit alors sous la forme :
\begin{equation*}
  a(A) = \{x \in E | Q(A, x)\}
\end{equation*}
Où $Q(A, x)$ est une formule logique sous la forme d'une DNF positive.

Puisque la fonction d'adhérence ne dépend que d'une combinaison de
relations de voisinages $\cal N$, l'objectif de notre algorithme
d'apprentissage est d'apprendre une DNF permettant de s'approcher de
la structure objectif à partir de $\cal N$.

% Dans le cas de notre exemple, plusieurs DNF permettent d'aboutir à la
% structure objectif en combinant nos trois relations de voisinages.
% ${\cal N}_1 \lor$

\subsection{Apprentissage par les fermés élémentaires}

Notre première approche consistait à modéliser le problème
d'apprentissage de la fonction d'adhérence dans une représentation
attribut/valeur. Chaque instance du problème est représentée par un couple
$(x, y) \in E \times E$ et possède autant
d'attributs que l'on dispose de relations de voisinages. La valeur de
l'attribut $i$ est positive si $x \in F_i(y)$, avec $F_i(y)$ le fermé de $y$ selon $N_i$. Enfin, une étiquette de
classe positive est attribuée aux instances satisfiant la structure
objectif.

\begin{table}[h]
  \centering

  \begin{tabular}{c | c | c | c | c}
    (x, y) & {\bfseries $N_1$}
    & {\bfseries $N_2$}
    & {\bfseries $N_3$}
    & {\bfseries Classe} $x \in F(y)$\\
    \hline
    (b, a) & 1 & 1 & 0 & + \\
    (c, a) & 0 & 1 & 0 & + \\
    % \rowcolor{yellow}
    (d, a) & 0 & 0 & 0 & + \\

    % \rowcolor{yellow}
    a, b) & 0 & 0 & 0 & - \\
    (c, b) & 0 & 0 & 1 & + \\
    (a, d) & 0 & 0 & 0 & - \\

    % (a, c) & 0 & 0 & 0 & - \\
    % (b, c) & 0 & 0 & 0 & - \\
    % (d, c) & 1 & 0 & 0 & + \\

    % (b, d) & 0 & 0 & 0 & - \\
    % (c, d) & 0 & 0 & 0 & - \\
    % (d, b) & 0 & 0 & 1 & + \\
  \end{tabular}

  \caption{Tableau d'apprentissage par la fermeture}
  \label{fig:tabapprentissage1}
\end{table}

Cependant, cette modélisation n'est pas satisfaisante. Un même espace
prétopologique peut être engendré par des fonctions d'adhérences
différentes. Or, cette propriété n'est pas représentable dans ce cadre d'apprentissage.
L'espace prétopologique de la figure \ref{fig:pretopoobjectif} peut
être engendré par plusieurs DNF :
\begin{align*}
  N_1 \lor N_2 \lor N_3\\
  N_1 \lor N_3 \lor (N_1 \land N_2)
\end{align*}
Le fermé élémentaire $F(a)$ peut alors être calculé d'au moins deux
façons :
\begin{align*}
  a(\{a\}) = \{a, b, c\} \rightarrow a^2(\{a\}) = \{a, b, c, d\}\\
  a(\{a\}) = \{a, b\} \rightarrow a^2(\{a\}) = \{a, b, c, d\}\\
\end{align*}

\subsection{Apprentissage par l'adhérence}

Notre seconde approche consiste à classer les instances selon leur
adhérence, et non leur fermeture.
% De plus, nous avons décidé de nous
% projetter dans un cadre multi-instances. Ce choix est motivé par le
% fait que deux fonctions d'adhérence différentes peuvent mener à la
% même structuration.

Le principal problème avec ce choix, c'est qu'il n'est pas possible de
prédire quel sera le résultat de la fonction d'adhérence. En effet,
nous connaissons la fermeture $F(a) = \{a, b, c, d\}$, mais cette
fermeture peut être calculée par différentes fonctions d'adhérence.

\begin{figure}[h]
  \centering

  \begin{align*}
    &a(\{a\}) = \{a, b\} \rightarrow a(a(\{a\})) = \{a, b, c, d\}&\\
    &a(\{a\}) = \{a, c\} \rightarrow a(a(\{a\})) = \{a, c, d\} \rightarrow a(a(a(\{a\}))) =
    \{a, b, c, d\}&
  \end{align*}

  \caption{Deux fonctions d'adhérence menant à la même structuration}
  \label{fig:memestructuration}
\end{figure}

La figure \ref{fig:memestructuration} montre deux fonctions d'adhérence
menant à la même structuration.

Cependant, nous savons que $a(\{a\}) \ne \{a\}$ (cf. figure
\ref{fig:pretopoobjectif}), donc $a(\{a\})$ contient au moins $b$, $c$
ou $d$, puisque, par définition d'un espace prétopologique, $A
\subseteq a(A)$.

\begin{equation*}
  b \in a(\{a\}) \lor c \in a(\{a\}) \lor d \in a(\{a\})
\end{equation*}

En restant dans un cadre supervisé classique, notre ensemble
d'apprentissage ressemblerait à celui illustré par le tableau
\ref{fig:tableauapprentissageadh1}.
Celui ci est plus complexe à apprendre, puisqu'une nouvelle classe
fait son apparition, et possède les mêmes inconvénients que la méthode précédentes.

\begin{table}[h]
  \centering

  \begin{tabular}{c | c | c | c | c}
    (x, y) & {\bfseries $N_1$}
    & {\bfseries $N_2$}
    & {\bfseries $N_3$}
    & {\bfseries Classe} $x \in a(\{y\})$\\
    \hline
    (b, a) & 1 & 1 & 0 & $\pm$\\
    (c, a) & 0 & 1 & 0 & $\pm$\\
    (d, a) & 0 & 0 & 0 & $\pm$\\

    (a, b) & 0 & 0 & 0 & - \\

    (c, b) & 0 & 0 & 1 & $\pm$ \\

    (a, d) & 0 & 0 & 0 & - \\

    (d, c) & 1 & 0 & 0 & +\\
  \end{tabular}

  \caption{Instances d'apprentissage supervisé classique par l'adhérence}
  \label{fig:tableauapprentissageadh1}
\end{table}

Le fermé élémentaire de $x \in E$ est le résultat des applications
successives de $a(.)$ sur $x$ jusqu'à obtention d'un point fixe.
Un fermé élémentaire peut alors exister sous plusieurs \emph{formes},
comme le montre la figure \ref{fig:memestructuration}. Puisque nous ne
nous intéressons uniquement à la structure finale, et non aux étapes
effectuées pour l'engendrer, nous devons traiter de la même façon les
différentes \emph{formes} des fermés élémentaires.

\subsection{Apprentissage supervisé multi-instances}

\subsubsection{Définition et travaux existants}

Dans le cadre de l'apprentissage multi-instances, l'ensemble
d'apprentissage est modéliser par un ensemble fini $\cal E$ de
\emph{sacs d'instances}, chaque sac d'instances $S_k$ contenant $n_k$
exemples d'un même objet sous différentes formes.

L'apprentissage multi-instances a pour la première fois été formalisé
par \citeauthor{dietterich1997solving}, dans le but de déterminer si
une molécule est apte à être utilisée dans la fabrication de
médicaments. Les molécules actives des médicaments agissent en
se liant au site actif d'une protéine. Or, ce site actif possède une
forme particulière, rendant ainsi difficile une liaison avec toutes molécules
ne respectant pas cette forme.

\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{shapes.png}
  \caption{Différentes configurations d'une même molécule}
\end{figure}

Une même molécule peut prendre différentes formes, par rotation de ses
liaisons simples. Une molécule dans une mauvaise configuration peut
alors être considérée, à tort, comme étant incompatible avec une
protéine.

Ce problème peut être résolu par l'apprentissage multi-instances. Dans
ce cadre, une molécule est représentée par un \emph{sac} contenant
différentes configurations de la même molécule. La molécule sera
classée positivement si au moins une des ses configurations est
compatible avec le site actif de la protéine cible.

\subsubsection{Application à l'apprentissage d'une fonction
  d'adhérence}

La fermeture élémentaire d'un élément $x \in E$ peut être obtenue par
différentes fonctions d'adfhérence $a(.)$. En d'autre termes, la
fermeture élémentaire de $x$ possède différentes \emph{formes}
possibles, chacune représentée par une fonction d'adhérence.

Cette définition de la fermeture élémentaire motive l'utilisation de
l'apprentissage supervisé multi-instances. Dans ce cadre, un sac
d'instances représente les couples $(y_i, x) \in E \times E$, avec $y_i$
le $i$-ème élément subsumé par $x$ dans la structure objectif $S$
($y_i \in F(x)$).

L'ensemble d'apprentissage utilisé avec cette méthode est illustré par
le tableau \ref{fig:tableauapprentissageadh}.

\begin{table}[h]
  \centering

  \begin{tabular}{c | c | c | c | c}
    (x, y) & {\bfseries $N_1$}
    & {\bfseries $N_2$}
    & {\bfseries $N_3$}
    & {\bfseries Classe} $x \in a(\{y\})$\\
    \hline
    (b, a) & 1 & 1 & 0 & \multirow{3}{*}{+} \\
    (c, a) & 0 & 1 & 0 &  \\
    (d, a) & 0 & 0 & 0 &  \\
    \hline

    (a, b) & 0 & 0 & 0 & - \\
    \hline
    (c, b) & 0 & 0 & 1 & + \\
    \hline
    (a, d) & 0 & 0 & 0 & - \\
  \end{tabular}

  \caption{Tableau d'apprentissage multi-instances par l'adhérence}
  \label{fig:tableauapprentissageadh}
\end{table}

On remarque qu'un sac d'instances possède plusieurs éléments
uniquement lorsque son label de classe est positif. En effet,
$\forall x \in E$, il y a ambiguïté sur le résultat de $a(\{x\})$ quand
$|F(\{x\})| > 2$ i.e. quand différents scénarios existent pour
atteindre le fermé de $x$.

Dans le cas contraire, si $|F(\{x\})| \leq 2$, il n'y a plus
d'ambiguïté. Si $x$ subsume $y$ (et seulement $y$), alors $F(\{x\}) = a(\{x\}) =
\{x, y\}$. Si $x$ ne subsume aucun élément, alors $F(\{x\}) = a(\{x\}) =
\{x\}$.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "rapport"
%%% End:
